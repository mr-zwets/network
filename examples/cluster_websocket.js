// Load the electrum library.
const { ElectrumCluster, ElectrumTransport } = require('../dist/main/lib/index');

// Wrap the application in an async function to allow use of await/async.
const main = async function()
{
	// Initialize an electrum cluster where 2 out of 3 needs to be consistent, polled randomly with fail-ove (default).
	const electrum = new ElectrumCluster('Electrum cluster example', '1.4.1', 2, 3);

	// Add some servers to the cluster.
	electrum.addServer('bch.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
	electrum.addServer('electroncash.de', 60002, ElectrumTransport.WSS.Scheme);
	electrum.addServer('blackie.c3-soft.com', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
	electrum.addServer('bch.loping.net', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);
	electrum.addServer('electrum.imaginary.cash', ElectrumTransport.WSS.Port, ElectrumTransport.WSS.Scheme);

	// Wait for enough connections to be available.
	await electrum.ready();

	// Declare an example transaction ID.
	const transactionID = '4db095f34d632a4daf942142c291f1f2abb5ba2e1ccac919d85bdc2f671fb251';

	// Request the full transaction hex for the transaction ID.
	const transactionHex = await electrum.request('blockchain.transaction.get', transactionID);

	// Print out the transaction hex.
	console.log(transactionHex);

	// Subscribe to block header notifications.
	await electrum.subscribe(console.log, 'blockchain.headers.subscribe');

	// Close all connections.
	electrum.shutdown();
};

// Run the application.
main();
