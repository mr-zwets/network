import debug from './util';
import ElectrumConnection from './electrum-connection';
import ElectrumProtocol from './electrum-protocol';
import { DefaultParameters } from './constants';
import { EventEmitter } from 'events';
import { ConnectionStatus } from './enums';
import { RPCParameter, isRPCNotification, isRPCErrorResponse, RPCNotification, RPCResponse } from './rpc-interfaces';
import type { ResolveFunction, RequestResolver, RequestResponse, TransportScheme } from './interfaces';
import { Mutex } from 'async-mutex';

/**
 * Triggers when the underlying connection is established.
 *
 * @event ElectrumClient#connected
 */

/**
 * Triggers when the underlying connection is lost.
 *
 * @event ElectrumClient#disconnected
 */

/**
 * Triggers when the remote server sends data that is not a request response.
 *
 * @event ElectrumClient#notification
 */

/**
 * High-level Electrum client that lets applications send requests and subscribe to notification events from a server.
 */
class ElectrumClient extends EventEmitter
{
	// Declare instance variables
	connection: ElectrumConnection;

	// Initialize an empty list of subscription metadata.
	subscriptionMethods: Record<string, Set<string>> = {};

	// Start counting the request IDs from 0
	requestId = 0;

	// Initialize an empty dictionary for keeping track of request resolvers
	requestResolvers: { [index: number]: RequestResolver } = {};

	// Mutex lock used to prevent simultaneous connect() and disconnect() calls.
	connectionLock = new Mutex();

	/**
	 * Initializes an Electrum client.
	 *
	 * @param {string} application            your application name, used to identify to the electrum host.
	 * @param {string} version                protocol version to use with the host.
	 * @param {string} host                   fully qualified domain name or IP number of the host.
	 * @param {number} port                   the TCP network port of the host.
	 * @param {TransportScheme} scheme        the transport scheme to use for connection
	 * @param {number} timeout                how long network delays we will wait for before taking action, in milliseconds.
	 * @param {number} pingInterval           the time between sending pings to the electrum host, in milliseconds.
	 * @param {number} reconnectInterval      the time between reconnection attempts to the electrum host, in milliseconds.
	 * @param {boolean} useBigInt			  whether to use bigint for numbers in json response.
	 *
	 * @throws {Error} if `version` is not a valid version string.
	 */
	constructor(
		application: string,
		version: string,
		host: string,
		port: number = DefaultParameters.PORT,
		scheme: TransportScheme = DefaultParameters.TRANSPORT_SCHEME,
		timeout: number = DefaultParameters.TIMEOUT,
		pingInterval: number = DefaultParameters.PING_INTERVAL,
		reconnectInterval: number = DefaultParameters.RECONNECT,
		useBigInt: boolean = DefaultParameters.USE_BIG_INT,
	)
	{
		// Initialize the event emitter.
		super();

		// Set up a connection to an electrum server.
		this.connection = new ElectrumConnection(application, version, host, port, scheme, timeout, pingInterval, reconnectInterval, useBigInt);
	}

	/**
	 * Connects to the remote server.
	 *
	 * @throws {Error} if the socket connection fails.
	 * @returns a promise resolving when the connection is established.
	 */
	async connect(): Promise<void>
	{
		// Create a lock so that multiple connects/disconnects cannot race each other.
		const unlock = await this.connectionLock.acquire();

		try
		{
			// If we are already connected, do not attempt to connect again.
			if(this.connection.status === ConnectionStatus.CONNECTED)
			{
				return;
			}

			// Listen for parsed statements.
			this.connection.on('statement', this.response.bind(this));

			// Hook up resubscription on connection.
			this.connection.on('connect', this.resubscribeOnConnect.bind(this));

			// Relay connect and disconnect events.
			this.connection.on('connect', this.emit.bind(this, 'connected'));
			this.connection.on('disconnect', this.onConnectionDisconnect.bind(this));

			// Relay error events.
			this.connection.on('error', this.emit.bind(this, 'error'));

			// Connect with the server.
			await this.connection.connect();
		}
		// Always release our lock so that we do not end up in a stuck-state.
		finally
		{
			unlock();
		}
	}

	/**
	 * Disconnects from the remote server and removes all event listeners/subscriptions and open requests.
	 *
	 * @param {boolean} force                 disconnect even if the connection has not been fully established yet.
	 * @param {boolean} retainSubscriptions   retain subscription data so they will be restored on reconnection.
	 *
	 * @returns true if successfully disconnected, or false if there was no connection.
	 */
	async disconnect(force: boolean = false, retainSubscriptions: boolean = false): Promise<boolean>
	{
		// Create a lock so that multiple connects/disconnects cannot race each other.
		const unlock = await this.connectionLock.acquire();

		try
		{
			if(!retainSubscriptions)
			{
				// Cancel all event listeners.
				this.removeAllListeners();

				// Remove all subscription data
				this.subscriptionMethods = {};
			}

			// For each pending request..
			for(const index in this.requestResolvers)
			{
				// Reject the request.
				const requestResolver = this.requestResolvers[index];
				requestResolver(new Error('Manual disconnection'));

				// Remove the request.
				delete this.requestResolvers[index];
			}

			// Disconnect from the remove server.
			return await this.connection.disconnect(force);
		}
		// Always release our lock so that we do not end up in a stuck-state.
		finally
		{
			unlock();
		}
	}

	/**
	 * Calls a method on the remote server with the supplied parameters.
	 *
	 * @param {string} method          name of the method to call.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise that resolves with the result of the method or an Error.
	 */
	async request(method: string, ...parameters: RPCParameter[]): Promise<Error | RequestResponse>
	{
		// If we are not connected to a server..
		if(this.connection.status !== ConnectionStatus.CONNECTED)
		{
			// Reject the request with a disconnected error message.
			throw(new Error(`Unable to send request to a disconnected server '${this.connection.host}'.`));
		}

		// Increase the request ID by one.
		this.requestId += 1;

		// Store a copy of the request id.
		const id = this.requestId;

		// Format the arguments as an electrum request object.
		const message = ElectrumProtocol.buildRequestObject(method, parameters, id);

		// Define a function to wrap the request in a promise.
		const requestResolver = (resolve: ResolveFunction<Error | RequestResponse>): void =>
		{
			// Add a request resolver for this promise to the list of requests.
			this.requestResolvers[id] = (error?: Error, data?: RequestResponse) =>
			{
				// If the resolution failed..
				if(error)
				{
					// Resolve the promise with the error for the application to handle.
					resolve(error);
				}
				else
				{
					// Resolve the promise with the request results.
					resolve(data as RequestResponse);
				}
			};

			// Send the request message to the remote server.
			this.connection.send(message);
		};

		// Write a log message.
		debug.network(`Sending request '${method}' to '${this.connection.host}'`);

		// return a promise to deliver results later.
		return new Promise<Error | RequestResponse>(requestResolver);
	}

	/**
	 * Subscribes to the method and payload at the server.
	 *
	 * @note the response for the subscription request is issued as a notification event.
	 *
	 * @param {string}    method       one of the subscribable methods the server supports.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise resolving when the subscription is established.
	 */
	async subscribe(method: string, ...parameters: RPCParameter[]): Promise<void>
	{
		// Initialize an empty list of subscription payloads, if needed.
		if(!this.subscriptionMethods[method])
		{
			this.subscriptionMethods[method] = new Set<string>();
		}

		// Store the subscription parameters to track what data we have subscribed to.
		this.subscriptionMethods[method].add(JSON.stringify(parameters));

		// Send initial subscription request.
		const requestData = await this.request(method, ...parameters);

		// Construct a notification structure to package the initial result as a notification.
		const notification =
		{
			jsonrpc: '2.0',
			method: method,
			params: [ ...parameters, requestData ],
		};

		// Manually emit an event for the initial response.
		this.emit('notification', notification);
	}

	/**
	 * Unsubscribes to the method at the server and removes any callback functions
	 * when there are no more subscriptions for the method.
	 *
	 * @param {string}    method       a previously subscribed to method.
	 * @param {...string} parameters   one or more parameters for the method.
	 *
	 * @throws {Error} if no subscriptions exist for the combination of the provided `method` and `parameters.
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise resolving when the subscription is removed.
	 */
	async unsubscribe(method: string, ...parameters: RPCParameter[]): Promise<void>
	{
		// Throw an error if the client is disconnected.
		if(this.connection.status !== ConnectionStatus.CONNECTED)
		{
			throw(new Error(`Unable to send unsubscribe request to a disconnected server '${this.connection.host}'.`));
		}

		// If this method has no subscriptions..
		if(!this.subscriptionMethods[method])
		{
			// Reject this promise with an explanation.
			throw(new Error(`Cannot unsubscribe from '${method}' since the method has no subscriptions.`));
		}

		// Pack up the parameters as a long string.
		const subscriptionParameters = JSON.stringify(parameters);

		// If the method payload could not be located..
		if(!this.subscriptionMethods[method].has(subscriptionParameters))
		{
			// Reject this promise with an explanation.
			throw(new Error(`Cannot unsubscribe from '${method}' since it has no subscription with the given parameters.`));
		}

		// Remove this specific subscription payload from internal tracking.
		this.subscriptionMethods[method].delete(subscriptionParameters);

		// Send unsubscription request to the server
		// NOTE: As a convenience we allow users to define the method as the subscribe or unsubscribe version.
		await this.request(method.replace('.subscribe', '.unsubscribe'), ...parameters);

		// Write a log message.
		debug.client(`Unsubscribed from '${String(method)}' for the '${subscriptionParameters}' parameters.`);
	}

	/**
	 * Restores existing subscriptions without updating status or triggering manual callbacks.
	 *
	 * @throws {Error} if subscription data cannot be found for all stored event names.
	 * @throws {Error} if the client is disconnected.
	 * @returns a promise resolving to true when the subscriptions are restored.
	 *
	 * @ignore
	 */
	private async resubscribeOnConnect(): Promise<void>
	{
		// Write a log message.
		debug.client(`Connected to '${this.connection.hostIdentifier}'.`);

		// Initialize an empty list of resubscription promises.
		const resubscriptionPromises = [];

		// For each method we have a subscription for..
		for(const method in this.subscriptionMethods)
		{
			// .. and for each parameter we have previously been subscribed to..
			for(const parameterJSON of this.subscriptionMethods[method].values())
			{
				// restore the parameters from JSON.
				const parameters = JSON.parse(parameterJSON);

				// Send a subscription request.
				resubscriptionPromises.push(this.subscribe(method, ...parameters));
			}

			// Wait for all re-subscriptions to complete.
			await Promise.all(resubscriptionPromises);
		}

		// Write a log message if there was any subscriptions to restore.
		if(resubscriptionPromises.length > 0)
		{
			debug.client(`Restored ${resubscriptionPromises.length} previous subscriptions for '${this.connection.hostIdentifier}'`);
		}
	}

	/**
	 * Parser messages from the remote server to resolve request promises and emit subscription events.
	 *
	 * @param {RPCNotification | RPCResponse} message   the response message
	 *
	 * @throws {Error} if the message ID does not match an existing request.
	 * @ignore
	 */
	response(message: RPCNotification | RPCResponse): void
	{
		// If the received message is a notification, we forward it to all event listeners
		if(isRPCNotification(message))
		{
			// Write a log message.
			debug.client(`Received notification for '${message.method}' from '${this.connection.host}'`);

			// Forward the message content to all event listeners.
			this.emit('notification', message);

			// Return since it does not have an associated request resolver
			return;
		}

		// If the response ID is null we cannot use it to index our request resolvers
		if(message.id === null)
		{
			// Throw an internal error, this should not happen.
			throw(new Error('Internal error: Received an RPC response with ID null.'));
		}

		// Look up which request promise we should resolve this.
		const requestResolver = this.requestResolvers[message.id];

		// If we do not have a request resolver for this response message..
		if(!requestResolver)
		{
			// Throw an internal error, this should not happen.
			throw(new Error('Internal error: Callback for response not available.'));
		}

		// Remove the promise from the request list.
		delete this.requestResolvers[message.id];

		// If the message contains an error..
		if(isRPCErrorResponse(message))
		{
			// Forward the message error to the request resolver and omit the `result` parameter.
			requestResolver(new Error(message.error.message));
		}
		else
		{
			// Forward the message content to the request resolver and omit the `error` parameter
			// (by setting it to undefined).
			requestResolver(undefined, message.result);
		}
	}

	/**
	 * Callback function that is called when connection to the Electrum server is lost.
	 * Aborts all active requests with an error message indicating that connection was lost.
	 *
	 * @ignore
	 */
	onConnectionDisconnect(): void
	{
		// Emit a disconnection signal to any listeners.
		this.emit('disconnected');

		// Loop over active requests
		for(const resolverId in this.requestResolvers)
		{
			// Extract request resolver for readability
			const requestResolver = this.requestResolvers[resolverId];

			// Resolve the active request with an error indicating that the connection was lost.
			requestResolver(new Error('Connection lost'));

			// Remove the promise from the request list.
			delete this.requestResolvers[resolverId];
		}
	}
}

// Export the client.
export default ElectrumClient;
